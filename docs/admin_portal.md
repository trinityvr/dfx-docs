# Admin Portal Overview

The [Admin Portal](https://portal.trinity-dfx.com) is the hub for **Coaches, Operators and Analysts** using the DiamondFX system.  The system is designed to allow an Operator to quickly configure and deploy a simulation on the cloud.
Operators will use the portal to create simulation scenarios for players.

---

## Login to the Portal

![Screenshot](img/admin_portal/login.png)

Every user of the DIAMONDFX system (excluding players) is provided with a [login](https://portal.trinity-dfx.com/#/account) linked to their organization account.  All users must [login](https://portal.trinity-dfx.com/#/account) to the portal prior to creating a simulation.

---

## Navigate to Home Page

![Screenshot](img/admin_portal/home.png)

Once signed in, proceed to the [DiamondFX Home Page](https://portal.trinity-dfx.com) and navigate to the app that reads [Sim Admin](https://portal.trinity-dfx.com/#/simulation).

---
## Simulation Administration

### Manage Teams
On the [Sim Admin](https://portal.trinity-dfx.com/#/teams) page, **Operators** can manage teams in the organization. From 
the [Teams](https://portal.trinity-dfx.com/#/teams) sidebar, **Operators** can *add, remove,* and *update* teams from 
a single interface.

![Screenshot](img/admin_portal/team_admin.png)

#### Add Teams
Using the **+** button below the **Teams** panel, an **Operator** can add a new team to their organization.  Fill in the form fields and
 submit to add the **Team** to the organization.

![Screenshot](img/admin_portal/add_team.png)

##### Team Fields
*   **Team Name**
*   **Team Type**
*   **Team Level**
*   **Team League**

#### Remove Teams
Select the **x** button next to the **player** you'd like to remove from the system to remove them.    

#### Update Teams
Select the **✐** button next to the **player** you'd like to update from the system to update them. You can edit the form fields 
and then select the *✔* button to save the entry.  

### Manage Players

On the [Sim Admin](https://portal.trinity-dfx.com/#/players) page, **Operators** can manage players using the system. From 
the [Players](https://portal.trinity-dfx.com/#/players) sidebar, **Operators** can *add, remove,* and *update* players from 
a single interface.

![Screenshot](img/admin_portal/player_admin.png)

#### Add Players
Using the **+** button below the **Players** panel, an **Operator** can add a new player to the organization.  Fill in the form fields and 
submit to add the **Player** to the organization.

![Screenshot](img/admin_portal/add_player.png)

##### Player Fields
*   **First Name**
*   **Last Name**
*   **BAM ID**
*   **Position**
*   **Team**

#### Remove Players
Select the **x** button next to the **player** you'd like to remove from the system to remove them.    

#### Update Players
Select the **✐** button next to the **player** you'd like to update from the system to update them. You can edit the form fields 
and then select the *✔* button to save the entry.  

### Create a Simulation

![Screenshot](img/admin_portal/sim_admin.png)

There are two tabs to define a **Player** and **Simulation** on the [Sim Admin](https://portal.trinity-dfx.com/#/simulation) page.

![Screenshot](img/admin_portal/Batter.png)

Enter the form data for the player in the tab that reads **Player**.  Player data is preloaded with every new DIAMONDFX account linked to the organization.  The following data fields can be entered for the "Player" tab:

#### Player Fields
*   **Player ID** - This is the player's MLB ID.  You can search and retrieve the player using the system by their MLB ID from this field and the remaining First Name and Last Name Fields will populate
*   **First Name** - This is the player's first name, it will auto populate if the player's MLB ID is entered
*   **Last Name** - This is the player's last name, it will auto populate if the player's MLB ID is entered
*   **Batter Type** - Select RHB if the player bats right-handed, and LHB if the player bats left handed
*   **Bat Length** - Select the length of the bat the player uses
*   **Bat Drop** - Select the drop weight in ounces of the bat the player uses

![Screenshot](img/admin_portal/pitcher.png)

Once the player form fields are complete, navigate to the **Simulation** tab.  This is the pitcher that the player will bat against or view pitches from in the simulation.

#### Simulation Fields

*   **Pitcher Name** - Search for the name of the pitcher you want to simulate.  DIAMONDFX will use a combination of PitchFX and Trackman data to simulate the pitcher's behaviors and trajectories
*   **Pitches** - Enter the # of pitches the player will see for a given session
*   **Pitch Speed** - Select the range of pitch speeds (min and max) that the player will see in a session
*   **Pitch Types** - Select one or multiple pitch types that the player will see in a session

### Launch the Simulation

The simulation is now ready to be deployed, select the **Create Simulation** button and the simulation will now be available in the [Simulation Client](sim_client.md).

---

## View Session Analytics

Once the session is created and the simulation is executed in the [Simulation Client](sim_client.md), you can view session analytics by selecting the **Session Analytics** button.
Keep in mind the session analytics are only available after the simulation is deployed and executed.
