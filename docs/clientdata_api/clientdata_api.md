# Client Data RESTful Web API Overview

The **Client Data API** is available for analysts who want to integrate DiamondFX application 
data within their own data transformation pipeline.  The RESTful APIs methods and routes 
are documented below and permissioned at the account level.  

---

## Authorization Header and Key

Every request to the **Client Data API** requires an authorization header with the associated account key. The account 
key is accessible in the [Sim Admin](https://portal.trinity-dfx.com/#/account.html) portal under the [Account](https://portal.trinity-dfx.com/account.html)
 page in the tab marked **"keys"**.


#### Sample Authorization Header

```
{Accept: "application/json", Authorization: "Bearer:[your key here]"}
```

---

## API Base URL

All requests are sent via https using the following base url:

```
https://api.trinity-dfx.com/clientdata
```

---

##  Accessing Batting Simulation Data

All associated data generated from [Batting Simulation](sim_client/#simulation-mode) mode is accessible via a single GET request.  Listed 
 below are the routes, methods, and sample output.  
 
### Routes, Methods, and Parameters

A single GET request to the following endpoint will return all Batting Simulation data associated with the account.
 
```
GET /battingsimulation
```

The following parameters can be passed on the query string:

| Parameter  | Datatype  | Format   | Required   | Description   |  
|---|---|---|---|---|
|*startDate*   |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Starting date boundary for the data pull   |   
|*endDate*     |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Ending date boundary for the data pull     |   
  
 
 
### Sample Output

The API response is returned in a JSON datastructure:
 
```
 [
  {
    "SwingData": {
      "AtBatId": 0,
      "PitchId": 0,
      "SwingId": 0,
      "Speed": 0,
      "SpeedMax": 0,
      "Type": "string",
      "SubType": "string",
      "MaxBallDistance": 0,
      "MaxBallHeight": 0,
      "Power": 0,
      "TimeOfImpact": 0,
      "AngleOfImpact": 0,
      "CollisionQ": 0,
      "CollisionOffsetX": 0,
      "BallExitSpeed": 0,
      "DeviceType": "string",
      "MountId": 0
    },
    "PitchData": {
      "PfxId": 0,
      "PfxGameId": "string",
      "AtBatId": 0,
      "Delivery": "string",
      "Type": "string",
      "SubType": "string",
      "Outcome": "string",
      "Speed": 0,
      "Date": "2018-02-01T19:34:23.178Z"
    },
    "SessionData": {
      "Id": 0,
      "Start": "2018-02-01T19:34:23.178Z",
      "Mode": "string",
      "SimId": 0,
      "DeviceType": "string",
      "MountId": 0,
      "Version": "string"
    },
    "AtBatId": {
      "Id": 0,
      "Date": "2018-02-01T19:34:23.178Z",
      "SessionId": 0,
      "PitcherId": "string",
      "BatterId": "string",
      "TeamId": "string"
    }
  }
]
```

### Batting Simulation Output Glossary

#### Swing Data

The **Batting Simulation** *SwingData* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*AtBatId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulated at bat   |   
|*PitchId*   |STRING  |N/A  | Unique DiamondFX identifier for the pitch thrown   |   
|*SwingId*   |STRING  |N/A   |Unique DiamondFX identifier for the swing |   
|*Speed*   |STRING  |N/A   |Average ppeed of the swing in MPH   |   
|*SpeedMax*   |STRING  |N/A   |Max speed of the swing in MPH   |   
|*Type*   |STRING  |N/A  |Type of swing   |   
|*Subtype*   |STRING  |N/A  |Subtype of the swing   |   
|*MaxBallDistance*   |DOUBLE  |N/A  |Max distance of a given hit per swing, measured in ft   |   
|*MaxBallHeight*   |DOUBLE  |N/A  | Max height of a given hit per swing, measured in ft   |   
|*Power*   |DOUBLE  |N/A  | Max power per swing, measured in Watts   |   
|*TimeOfImpact*   |DOUBLE  |N/A  |Start of forward motion to the moment of impact, measured in seconds   |   
|*AngleOfImpact*   |DOUBLE  |N/A  |Angle of the bat’s path, at impact, relative to horizontal, measured in degrees   |   
|*CollisionQ*   |DOUBLE  |N/A  |Collision efficiency of the bat to ball   |   
|*CollisionOffsetX*   |DOUBLE  |N/A  |x offset for collision efficiency of the bat to ball   |   
|*BallExitSpeed*   |DOUBLE  |N/A  |Exit speed of the ball at impact per swing, measured in mph   |   
|*DeviceType*   |STRING  |N/A  |Type of input tracking device used   |   
|*MountId*   |INTEGER  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking   |   

#### Pitch Data

The **Batting Simulation** *PitchData* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PfxId*   |INTEGER  |N/A   |PitchFX event ID for the pitch thrown   |   
|*PfxGameId*   |STRING  |N/A  | String identifier for the game where the original pitch was thrown   |   
|*Delivery*   |STRING  |N/A   |String indication of the delivery type     |   
|*Type*   |STRING  |N/A   |String indication of the pitch type   |   
|*Speed*   |DOUBLE  |N/A  |Speed of the pitch in MPH   |   

#### Session Data

The **Batting Simulation** *SessionData* object response data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the session   |   
|*Start*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the start of the session   |   
|*Mode*   |STRING  |N/A   |String identifier for the simulation mode selected      |   
|*SimId*   |Integer  |N/A   |Unique DiamondFX identifier for the simulation    |   
|*DeviceType*   |DOUBLE  |N/A  |Type of input of input tracking device used   |   
|*MountId*   |DOUBLE  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking  (batting simulation relevant only)   |   
|*Version*   |DOUBLE  |N/A  |Identifier for DiamondFX application version   |   

#### At Bat Data

The **Batting Simulation** *AtBatId* object response data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*Date*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the creation of the simulation   |   
|*SessionId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation      |   
|*PitcherId*   |STRING  |N/A   |String BAM Identifier for the pitcher in the simulation    |   
|*BatterId*   |STRING  |N/A  |String name of the batter in the simulation   |   
|*TeamId*   |String  |N/A  |String name of the team assigned to the player in the organization   | 

---

##  Accessing Pitch Recognition Data

All associated data generated from [Pitch Recognition](sim_client/#simulation-mode) mode is accessible via a single **GET** request.  Listed 
 below are the routes, methods, and sample output.
 
### Routes, Methods, and Parameters

A single GET request to the following endpoint will return all Pitch Recognition data associated with the account.
 
```
GET /pitchrecognition
```

The following parameters can be passed on the query string:

| Parameter  | Datatype  | Format   | Required   | Description   |  
|---|---|---|---|---|
|*startDate*   |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Starting date boundary for the data pull   |   
|*endDate*     |DATETIME   |YYYY-MM-DD HH:MM:SS   |*✔*   |Ending date boundary for the data pull     |   

### Sample Output

The API response is returned in a JSON data structure:

```

[
  {
    "CallData": {
      "PitchId": 0,
      "Score": 0,
      "ReactionTime": 0,
      "TrackedTime": 0
    },
    "ExerciseInfo": {
      "ConfigId": 0
    },
    "ExerciseData": {
      "AtBatId": 0,
      "PitchSpeedScalar": 0,
      "TrajectoryCutoff": 0,
      "PitchBreak": true,
      "PitchAvatar": true
    },
    "PitchData": {
      "PfxId": 0,
      "PfxGameId": "string",
      "AtBatId": 0,
      "Delivery": "string",
      "Type": "string",
      "SubType": "string",
      "Outcome": "string",
      "Speed": 0,
      "Date": "2018-02-01T19:34:23.182Z"
    },
    "SessionData": {
      "Id": 0,
      "Start": "2018-02-01T19:34:23.182Z",
      "Mode": "string",
      "SimId": 0,
      "DeviceType": "string",
      "MountId": 0,
      "Version": "string"
    },
    "AtBatId": {
      "Id": 0,
      "Date": "2018-02-01T19:34:23.182Z",
      "SessionId": 0,
      "PitcherId": "string",
      "BatterId": "string",
      "TeamId": "string"
    }
  }
]

```

### Pitch Recognition Output Glossary

#### Call Data

The **Pitch Recognition** *Call* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PitchId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the pitch thrown   |   
|*Score*     |INTEGER   |N/A                |1 for correct calls, 0 for incorrect       | 
|*ReactionTime*     |FLOAT   |N/A   |Reaction time in milliseconds from the end of the pitch, a (-) reaction time indicates the pitch was called before the end of the end of the pitch     | 
|*TrackedTime*     |FLOAT   |N/A   |Time in milliseconds the ball is within player central cone of vision     |
 
#### Exercise Info Data

The **Pitch Recognition** *ExerciseInfo* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*ConfigId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the exercise configuration   |   

The **Pitch Recognition** *ExerciseData* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*AtBatId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*PitchSpeedScalar*   |DOUBLE  |N/A   | Scalar applied to speed of the pitch thrown   |   
|*TrajectoryCutoff*   |INTEGER  |N/A   |Feet from home plate the trajectory was occluded   |   
|*PitchBreak*   |BOOLEAN  |N/A   |Indication of whether the break of the pitch was rendered   |   
|*PitchAvatar*   |BOOLEAN  |N/A  |Indication of whether the break of the pitch was rendered   |   

The **Pitch Recognition** *PitchData* object response data is as follows:

| Parameter  | Datatype  | Format| Description   |  
|---|---|---|---|
|*PfxId*   |INTEGER  |N/A   |PitchFX event ID for the pitch thrown   |   
|*PfxGameId*   |STRING  |N/A  | String identifier for the game where the original pitch was thrown   |   
|*Delivery*   |STRING  |N/A   |String indication of the delivery type     |   
|*Type*   |STRING  |N/A   |String indication of the pitch type   |   
|*Speed*   |DOUBLE  |N/A  |Speed of the pitch in MPH   |   

#### Session Data

The **Pitch Recognition** *SessionData* object response data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the session   |   
|*Start*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the start of the session   |   
|*Mode*   |STRING  |N/A   |String identifier for the simulation mode selected      |   
|*SimId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation    |   
|*DeviceType*   |DOUBLE  |N/A  |Type of input of input tracking device used   |   
|*MountId*   |DOUBLE  |N/A  |DiamondFX unique identifier for type of mount used for bat tracking (batting simulation relevant only)   |   
|*Version*   |DOUBLE  |N/A  |Identifier for DiamondFX application version   |   

#### At Bat Data

The **Pitch Recognition** *AtBatId* object response data is as follows:

| Parameter  | Datatype  | Format | Description   |  
|---|---|---|---|
|*Id*   |INTEGER  |N/A   |Unique DiamondFX identifier for the at bat   |   
|*Date*   |DATETIME  |YYYY-MM-DD HH:MM:SS  | Datetime object for the creation of the simulation   |   
|*SessionId*   |INTEGER  |N/A   |Unique DiamondFX identifier for the simulation      |   
|*PitcherId*   |STRING  |N/A   |String BAM Identifier for the pitcher in the simulation    |   
|*BatterId*   |STRING  |N/A  |String name of the batter in the simulation   |   
|*TeamId*   |STRING  |N/A  |String name of the team assigned to the player in the organization   |   

---
