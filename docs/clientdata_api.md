# Client Data API Overview

The **Client Data API** is available for analysts who want to integrate DiamondFX application 
data within their own data transformation pipeline.  The RESTful APIs methods and routes 
are documented below and permissioned at the account level.  

---

## Authorization Header and Key

Every request to the **Client Data API** requires an authorization header with the associated account key. The account 
key is accessible in the [Sim Admin](https://portal.trinity-dfx.com/account.html) portal under the [Account](https://portal.trinity-dfx.com/account.html)
 page in the tab marked **"keys"**.


#### Sample Authorization Header

```
{Accept: "application/json", Authorization: "Bearer:[your key here]"}
```

---

## API Base URL

All requests are sent via https using the following base url:

```
https://api.trinity-dfx.com/clientdata
```

---

##  Accessing Batting Simulation Data

All associated data generated from [Batting Simulation](sim_client/#simulation-mode) mode is accessible via a single GET request.  Listed 
 below are the routes, methods, and sample output.  
 
### Routes, Methods, and Parameters

A single GET request to the following endpoint will return all Batting Simulation data associated with the account.
 
```
GET /battingsimulation
```

The following parameters can be passed on the query string:

| Parameter  | Datatype  | Required   |Description   |  
|---|---|---|---|
|*startDate*   |DATETIME   |*✔*   |Starting date boundary for the data pull   |   
|*endDate*   |DATETIME   |*✔*   |Ending date boundary for the data pull   |   
  
 
 
### Sample Output

The API response is returned in a JSON datastructure:
 
```
 [
  {
    "SwingData": {
      "AtBatId": 0,
      "PitchId": 0,
      "SwingId": 0,
      "Speed": 0,
      "SpeedMax": 0,
      "Type": "string",
      "SubType": "string",
      "MaxBallDistance": 0,
      "MaxBallHeight": 0,
      "Power": 0,
      "TimeOfImpact": 0,
      "AngleOfImpact": 0,
      "CollisionQ": 0,
      "CollisionOffsetX": 0,
      "BallExitSpeed": 0,
      "DeviceType": "string",
      "MountId": 0
    },
    "PitchData": {
      "PfxId": 0,
      "PfxGameId": "string",
      "AtBatId": 0,
      "Delivery": "string",
      "Type": "string",
      "SubType": "string",
      "Outcome": "string",
      "Speed": 0,
      "Date": "2018-02-01T19:34:23.178Z"
    },
    "SessionData": {
      "Id": 0,
      "Start": "2018-02-01T19:34:23.178Z",
      "Mode": "string",
      "SimId": 0,
      "DeviceType": "string",
      "MountId": 0,
      "Version": "string"
    },
    "AtBatId": {
      "Id": 0,
      "Date": "2018-02-01T19:34:23.178Z",
      "SessionId": 0,
      "PitcherId": "string",
      "BatterId": "string",
      "TeamId": "string"
    }
  }
]
```


##  Accessing Pitch Recognition Data

All associated data generated from [Pitch Recognition](sim_client/#simulation-mode) mode is accessible via a single **GET** request.  Listed 
 below are the routes, methods, and sample output.
 
### Routes, Methods, and Parameters

A single GET request to the following endpoint will return all Pitch Recognition data associated with the account.
 
```
GET /pitchrecognition
```

The following parameters can be passed on the query string:

| Parameter  | Datatype  | Required   |Description   |  
|---|---|---|---|
|*startDate*   |DATETIME   |*✔*   |Starting date boundary for the data pull   |   
|*endDate*   |DATETIME   |*✔*   |Ending date boundary for the data pull   | 

### Sample Output

The API response is returned in a JSON datastructure:

```

[
  {
    "CallData": {
      "PitchId": 0,
      "Score": 0,
      "ReactionTime": 0,
      "TrackedTime": 0
    },
    "ExerciseInfo": {
      "ConfigId": 0
    },
    "ExerciseData": {
      "AtBatId": 0,
      "PitchSpeedScalar": 0,
      "TrajectoryCutoff": 0,
      "PitchBreak": true,
      "PitchAvatar": true
    },
    "PitchData": {
      "PfxId": 0,
      "PfxGameId": "string",
      "AtBatId": 0,
      "Delivery": "string",
      "Type": "string",
      "SubType": "string",
      "Outcome": "string",
      "Speed": 0,
      "Date": "2018-02-01T19:34:23.182Z"
    },
    "SessionData": {
      "Id": 0,
      "Start": "2018-02-01T19:34:23.182Z",
      "Mode": "string",
      "SimId": 0,
      "DeviceType": "string",
      "MountId": 0,
      "Version": "string"
    },
    "AtBatId": {
      "Id": 0,
      "Date": "2018-02-01T19:34:23.182Z",
      "SessionId": 0,
      "PitcherId": "string",
      "BatterId": "string",
      "TeamId": "string"
    }
  }
]

```