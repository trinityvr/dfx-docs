# Analytics Portal Overview

The analytics portal is where Analysts can explore simulation session data with a high level interface.
Analysts can compare any number of players in the same view, and view high level descriptive statistics around a player's individual performance.
In this section, we'll explain the types of *charts, metrics, and filters* available in the portal.

___

## Navigate to the Analytics Portal

From the [Admin Portal](https://portal.trinity-dfx.com) home page, select the Analytics app launch button.

![Screenshot](img/analytics/analytics.png)

**Please note**, you must be logged in before visiting the Analytics portal, and you must launch the app from the [home](https://portal.trinity-dfx.com) page.

___

## Data Filters Summary

The data filters interface is located to the left of the charting window in the analytics view.  The filters allow for fine-grained filtering of data and players based on key attributes.

![Screenshot](img/analytics/Filters.png)


### Date Range Filter
*   **Start Date:** Select the start date to filter the data against
*   **End Date:** Select the end date to filter the data against

### Main Filters

All main filter fields are multi-selectable.  If you know what items you want to add or remove in each field, select the item and delete to remove, or start typing to find an item to add.

![Screenshot](img/analytics/main_filters.png)


*   **Simulation Mode:** Select the simulation mode(s) to filter the data against.  Simulation modes include:
    *   Batting Simulation only:
        *   Batting Practice
        *   Pitch Sequence
        *   At Bat
    *   Pitch Recognition only:
        *   Pitch Identification
        *   Strike Identification
*   **Team:** Displays all data associated with you account by default.
*   **Batter:** Select the batter(s) to filter the data against.
*   **Pitcher Throws:**  Select the pitcher handedness to filter the data against.


### Sub Filters

All sub filter fields are multi-selectable.  If you know what items you want to add or remove in each field, select the item and delete to remove, or start typing to find an item to add.

![Screenshot](img/analytics/sub_filters.png)

*   **Pitcher**  Select the pitcher(s) to filter the data against.

*   **Pitch Type:** Select the pitch type(s) to filter the data against.  Pitch type options include:
    *   Sinker
    *   Curveball
    *   Eephus
    *   Slider
    *   4-Seam
    *   Cutter
    *   Change-up
    *   2-Seam
    *   Fastball
    *   Knuckleball

#### Batting Simulation Only Subfilters

*   **Swing Type:** Select swing type(s) to filter the data against.  Swing type options include:
    *   Hit
    *   Ball
    *   Strike
    *   Miss
    *   Swinging Strike
    *   Called Strike
    
*   **Swing Subtype:** Select swing subtype(s) to filter the data against.  Swing type options include:
    *   Groundout
    *   Single
    *   Flyout
    *   Foul
    *   Double
    *   Strike
    *   Triple
    *   Ball

#### Pitch Recognition Only Subfilters

*   **Pitch Speed Scalar:** Select the pitch speed scalar configuration(s) to filter the data against.  Pitch speed scalar options include:
    *   1 = 100% speed
    *   0.5 = 50% speed

*   **Trajectory Cutoff:** Select the trajectory cutoff configuration(s) to filter the data against. Trajectory cutoff options include:
    *   0ft = Trajectory 0ft from the mound
    *   40ft = Trajectory is cutoff 40 ft from the mound

*   **Pitch Break:** Select the pitch break configuration(s) to filter the data against.  Pitch break options include:
    *   1 = Pitch break was rendered
    *   0 = Pitch break was not rendered

*   **Pitch Avatars:** Select the pitch avatar configuration(s) to filter the data against.  Pitch avatar options include
    *   1 = Avatar was rendered
    *   0 = Avatar was not rendered
___

## Chart Types Summary

The key charts in the DIAMONDFX Analytics portal are intended to allow for high level descriptive analysis of a player's swing, hit, and pitch recognition dynamics.
Using these tools, an analyst can quickly assess comparitive player norms across key metrics.  

*   **Guage Chart:**  The gauge chart in each view summarizes the overall average for the selected metric.
![Screenshot](img/analytics/guagechart.png)  

*   **Bar Chart:**  The bar plot in each view visualizes player averages for the selected metric.  *For more information on bar charts, please look [here](https://en.wikipedia.org/wiki/Bar_chart).*  
![Screenshot](img/analytics/barplot.png)  

*   **2D Scatter Plot:**  The scatter plot visualizes 2 continuous variables on an x and y axis.  *For more information on scatter plots, please look [here](https://en.wikipedia.org/wiki/Scatter_plot).*
![Screenshot](img/analytics/scatter.png)  

*   **Density Plot:**  The density plot visualizes the relative smooth distribution of data for a given metric along the x-axis.  *For more information on density plots, please look [here](https://en.wikipedia.org/wiki/Density_estimation).*
![Screenshot](img/analytics/density.png)  

*   **Box Plot:**  The box plot visualizes the distribution of the data with an indication of median, error, and upper and lower quartiles along the y-axis. *For more information on box plots, please look [here](https://en.wikipedia.org/wiki/Box_plot).*
![Screenshot](img/analytics/boxplot.png)  
  
___

## Batting Simulation Analytics

The **Batting Simulation** analytics tab is selected by default, within the batting simulation tab, there are few sub tabs to explore different batting metrics.
Batting simulation data is associated with all players who performed the **Batting Simulation** exercises in the [Sim Client](sim_client.md)

### Swing Metrics

![Screenshot](img/analytics/swing_analysis.png)

*   **Power:** Max power per swing, measured in *Watts*
*   **Max Distance:** Max distance of a given hit per swing, measured in *ft*
*   **Max Height:** Max height of a given hit per swing, measured in *ft*
*   **Time of Impact:** Start of forward motion to the moment of impact, measured in *seconds*
*   **Angle of Impact:** Angle of the bat’s path, at impact, relative to horizontal, measured in *degrees*
*   **Swing Speed:** Average speed of the swing, measured in *mph*
*   **Exit Speed:** Exit speed of the ball at impact per swing, measured in *mph*
*   **Custom Charts:**  Custom 2D scatter plots can be defined here.  Configurable parameters include:
    *   **X-axis:**  The x-axis of the continuous or the discrete variable
    *   **Y-axis:**  The y-axis of the continuous or discrete variable
    *   **Grouping:** The variable grouping of the chart

### Hit Metrics

![Screenshot](img/analytics/hit_analysis.png)

*   **Swing Type Distributions:** Density and box plot visualizations of a player's swing type distributions.  Sub plots include:
    *   Hit Distribution
    *   Ball Distribution
    *   Strike Distribution
    *   Miss Distribution
    *   Swinging Strike Distribution
    *   Called Strike Distribution
*   **Swing Subtype Distributions:** Density box plot visualizations of a player's swing subtype distributions.  Sub plots include:
    *   Groundout Distribution
    *   Single Distribution
    *   Flyout Distribution
    *   Foul Distribution
    *   Double Distribution
    *   Strike Distribution
    *   Triple Distribution
    *   Ball Distribution


### Diamond Analysis (Experimental)

![Screenshot](img/analytics/diamond_analysisPNG.png)

*   **Leaderboard:** Composite ranking of player swing and batting metric aggregates
    *   A *score of 0* indicates a player is perfectly average across all attributes
    *   A *positive score* indicates the player is above average across all or some attributes
    *   A *negative score* indicates the player is below average across all or some attributes
*   **Heatmap:**  Heatmap visualization of player swing and batting metric aggregates
*   **3D Scatter:** 3D scatter visualization of player swing and batting metric aggregates

___

## Pitch Recognition Analytics

The **Pitch Recognition** analytics tab can be selected, within the **Pitch Recognition** tab, there are few sub tabs to explore different pitch recognition metrics.
Batting simulation data is associated with all players who performed the **Pitch Recognition** exercises in the [Sim Client](sim_client.md).

![Screenshot](img/analytics/recall_analysis.png)


### Recall Metrics

*   **Recall Precision:** Indicates distribution of *correct* versus *incorrect* pitches called by pitch
*   **Recall Speed:** Indicates distribution of recall speed in *seconds* by pitch
*   **Recall Tracking:** Indicates distribution of time a called pitch was tracked in *seconds* by pitch
        