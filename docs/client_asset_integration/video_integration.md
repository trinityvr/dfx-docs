# Video Integration Overview

Teams aggregrating pitcher video files can integrated their assets within their 
shared DiamondFX account.  Video assets are permissioned and made available within your organization exclusively and are hosted server-side.

### Video Asset Sharing

To start integrating your video assets within your account today, complete the following:

1.  Create a BOX.com account 
2.  Create a shared BOX.com folder with the outlined directory / folder / file naming conventions
3.  Upload files with outlined naming and format requirements
4.  Share the folder with *julian@trinityvr.com* 

### Video Asset Preferred Folder Naming Scheme

The folder you create to contain your Video assets should reflect the following naming convention:

``` 
   TeamAbbreviation-PitcherVideo 
```

### Video Asset Preferred File Naming Scheme

Key pieces of metadata should be embedded in the file name for easy attribution.  The following pieces of metadata are required for proper indexing:

| Metadata  | Description  |  
|---|---|
|*pitcherid*   |The BAM ID of the pitcher   |
|*svid*   |The sv_id of the pitch thrown   |
|*gameid*   |the game_id of the pitch thrown   |
|*pitchtype*   |The pitch type abbreviation of the pitch thrown   |
|*epochtime* | Epochtime for the file creation date |

```
pitcherid_svid_gameid_pitchtype_epochtime.[mp4/.mov/.avi]
```

### Video Asset Guidelines

| Attribute  | Requirement  |  
|---|---|
|**Format**    |.mp4, .mov, .avi file types supported   |
|**Resolution**   |1920x1080 and above   |
|**Framerate**   |60fps and above (not interpolated) recommended  |

*   **Minimum recommended 2 clips per pitcher per pitch type**
    *   *Right-side view of the pitcher (if the batter is left-handed)*
    *   *Left-side view of the pitcher (if the batter is right handed)*
    *   *Footage is clipped from rest to wind-up, to delivery, back to rest*
    
*   **Other recommendations:**
    *   *Footage shot at roughly eye-level*
    *   *Camera should be static and not pan / zoom*
    *   *Footage shot from angle that minimizes occlusions to the pitcher (e.g. catcher, batter)*
    *   *Footage cropped to same size for all pitchers / pitch types, with most of the background and foreground cropped*   

