# Biomechanics Integration Overview

Teams aggregrating biomechanics data from systems such as **Simi** and **Kinatrax** can integrate their datasets within their 
shared DiamondFX account.  Biomechanics assets are permissioned and made available within your organization exclusively and are hosted server-side.

### Biomechanics Asset Sharing

To start integrating your biomechanics assets within your account today, complete the following:

1.  Create a BOX.com account 
2.  Create a shared BOX.com folder with the outlined directory / folder / file naming conventions
3.  Upload files with outlined naming and format requirements
4.  Share the folder with *julian@trinityvr.com* 

### Biomechanics Asset Preferred Folder Naming Scheme

The folder you create to contain your Biomechanics assets should reflect the following naming convention:

``` 
   TeamAbbreviation-PitcherBiomechanics 
```

### Biomechanics Asset Preferred File Naming Scheme

Key pieces of metadata should be embedded in the file name for easy attribution.  The following pieces of metadata are required for proper indexing:

| Metadata  | Description  |  
|---|---|
|*pitcherid*   |The BAM ID of the pitcher   |
|*svid*   |The sv_id of the pitch thrown   |
|*gameid*   |the game_id of the pitch thrown   |
|*pitchtype*   |The pitch type abbreviation of the pitch thrown   |
|*biosource*   |Enumeration for the capture source (KTX or SIMI)   |
|*capturerate* | Integer value for the capture rate of the source |
|*epochtime* | Epochtime for the file creation date |

```
pitcherid_svid_gameid_pitchtype_biosource_capturerate_epochtime.csv

```

### Kinematics Segment Hierarchy

![Screenshot](img/client_assets/segment_bones.png)


```
Hips (root segment)
-----JointSpine
----------Spine
---------------Neck
--------------------JointHead
-------------------------Head
--------------------JointLeftUpperArm
-------------------------LeftUpperArm
------------------------------LeftForearm
-----------------------------------LeftWrist
----------------------------------------LeftHand
--------------------JointRightUpperArm
-------------------------RightUpperArm
------------------------------RightForearm
-----------------------------------RightWrist
----------------------------------------RightHand
-----JointLeftThigh
----------LeftThigh
---------------LeftShin
--------------------LeftAnkle
-------------------------LeftFoot
-----JointRightThigh
----------RightThigh
---------------RightShin
--------------------RightAnkle
-------------------------RightFoot
```

### Biomechanics Asset Example File 

Each file should contain the global transformation matrix for each segment, and every row of data should correspond to a video frame.  The timestamp should be relative to the start of capture and provided in the first column.

The 4x4 3D rigid transformation matrix should be provided in columwise-order with the following standard format:

```
[R t; 0 1]
```

The transformation matrix should be composed of a rotation matrix, R and translation vector, t:

``` 
    R11 R12 R13 tx
    R21 R22 R23 ty
    R31 R32 R33 tz
    0 0 0 1
```


#### Header Example

```
Timestamp,Hips_R11 Hips_R12 Hips_R13 Hips_tx Hips_R21 Hips_R22 Hips_R23 Hips_ty Hips_R31 Hips_R32 Hips_R33 Hips_tz Hips_M41 Hips_M42 Hips_M43 Hips_M44 JointSpine_R11 JointSpine_R12 JointSpine_R13 JointSpine_tx JointSpine_R21 JointSpine_R22 JointSpine_R23 JointSpine_ty JointSpine_R31 JointSpine_R32 JointSpine_R33 JointSpine_tz JointSpine_M41 JointSpine_M42 JointSpine_M43 JointSpine_M44 Spine_R11 Spine_R12 Spine_R13 Spine_tx Spine_R21 Spine_R22 Spine_R23 Spine_ty Spine_R31 Spine_R32 Spine_R33 Spine_tz Spine_M41 Spine_M42 Spine_M43 Spine_M44 Neck_R11 Neck_R12 Neck_R13 Neck_tx Neck_R21 Neck_R22 Neck_R23 Neck_ty Neck_R31 Neck_R32 Neck_R33 Neck_tz Neck_M41 Neck_M42 Neck_M43 Neck_M44 JointHead_R11 JointHead_R12 JointHead_R13 JointHead_tx JointHead_R21 JointHead_R22 JointHead_R23 JointHead_ty JointHead_R31 JointHead_R32 JointHead_R33 JointHead_tz JointHead_M41 JointHead_M42 JointHead_M43 JointHead_M44 Head_R11 Head_R12 Head_R13 Head_tx Head_R21 Head_R22 Head_R23 Head_ty Head_R31 Head_R32 Head_R33 Head_tz Head_M41 Head_M42 Head_M43 Head_M44 JointLeftUpperArm_R11 JointLeftUpperArm_R12 JointLeftUpperArm_R13 JointLeftUpperArm_tx JointLeftUpperArm_R21 JointLeftUpperArm_R22 JointLeftUpperArm_R23 JointLeftUpperArm_ty JointLeftUpperArm_R31 JointLeftUpperArm_R32 JointLeftUpperArm_R33 JointLeftUpperArm_tz JointLeftUpperArm_M41 JointLeftUpperArm_M42 JointLeftUpperArm_M43 JointLeftUpperArm_M44 LeftUpperArm_R11 LeftUpperArm_R12 LeftUpperArm_R13 LeftUpperArm_tx LeftUpperArm_R21 LeftUpperArm_R22 LeftUpperArm_R23 LeftUpperArm_ty LeftUpperArm_R31 LeftUpperArm_R32 LeftUpperArm_R33 LeftUpperArm_tz LeftUpperArm_M41 LeftUpperArm_M42 LeftUpperArm_M43 LeftUpperArm_M44 LeftForearm_R11 LeftForearm_R12 LeftForearm_R13 LeftForearm_tx LeftForearm_R21 LeftForearm_R22 LeftForearm_R23 LeftForearm_ty LeftForearm_R31 LeftForearm_R32 LeftForearm_R33 LeftForearm_tz LeftForearm_M41 LeftForearm_M42 LeftForearm_M43 LeftForearm_M44 LeftWrist_R11 LeftWrist_R12 LeftWrist_R13 LeftWrist_tx LeftWrist_R21 LeftWrist_R22 LeftWrist_R23 LeftWrist_ty LeftWrist_R31 LeftWrist_R32 LeftWrist_R33 LeftWrist_tz LeftWrist_M41 LeftWrist_M42 LeftWrist_M43 LeftWrist_M44 LeftHand_R11 LeftHand_R12 LeftHand_R13 LeftHand_tx LeftHand_R21 LeftHand_R22 LeftHand_R23 LeftHand_ty LeftHand_R31 LeftHand_R32 LeftHand_R33 LeftHand_tz LeftHand_M41 LeftHand_M42 LeftHand_M43 LeftHand_M44 JointRightUpperArm_R11 JointRightUpperArm_R12 JointRightUpperArm_R13 JointRightUpperArm_tx JointRightUpperArm_R21 JointRightUpperArm_R22 JointRightUpperArm_R23 JointRightUpperArm_ty JointRightUpperArm_R31 JointRightUpperArm_R32 JointRightUpperArm_R33 JointRightUpperArm_tz JointRightUpperArm_M41 JointRightUpperArm_M42 JointRightUpperArm_M43 JointRightUpperArm_M44 RightUpperArm_R11 RightUpperArm_R12 RightUpperArm_R13 RightUpperArm_tx RightUpperArm_R21 RightUpperArm_R22 RightUpperArm_R23 RightUpperArm_ty RightUpperArm_R31 RightUpperArm_R32 RightUpperArm_R33 RightUpperArm_tz RightUpperArm_M41 RightUpperArm_M42 RightUpperArm_M43 RightUpperArm_M44 RightForearm_R11 RightForearm_R12 RightForearm_R13 RightForearm_tx RightForearm_R21 RightForearm_R22 RightForearm_R23 RightForearm_ty RightForearm_R31 RightForearm_R32 RightForearm_R33 RightForearm_tz RightForearm_M41 RightForearm_M42 RightForearm_M43 RightForearm_M44 RightWrist_R11 RightWrist_R12 RightWrist_R13 RightWrist_tx RightWrist_R21 RightWrist_R22 RightWrist_R23 RightWrist_ty RightWrist_R31 RightWrist_R32 RightWrist_R33 RightWrist_tz RightWrist_M41 RightWrist_M42 RightWrist_M43 RightWrist_M44 RightHand_R11 RightHand_R12 RightHand_R13 RightHand_tx RightHand_R21 RightHand_R22 RightHand_R23 RightHand_ty RightHand_R31 RightHand_R32 RightHand_R33 RightHand_tz RightHand_M41 RightHand_M42 RightHand_M43 RightHand_M44 JointLeftThigh_R11 JointLeftThigh_R12 JointLeftThigh_R13 JointLeftThigh_tx JointLeftThigh_R21 JointLeftThigh_R22 JointLeftThigh_R23 JointLeftThigh_ty JointLeftThigh_R31 JointLeftThigh_R32 JointLeftThigh_R33 JointLeftThigh_tz JointLeftThigh_M41 JointLeftThigh_M42 JointLeftThigh_M43 JointLeftThigh_M44 LeftThigh_R11 LeftThigh_R12 LeftThigh_R13 LeftThigh_tx LeftThigh_R21 LeftThigh_R22 LeftThigh_R23 LeftThigh_ty LeftThigh_R31 LeftThigh_R32 LeftThigh_R33 LeftThigh_tz LeftThigh_M41 LeftThigh_M42 LeftThigh_M43 LeftThigh_M44 LeftShin_R11 LeftShin_R12 LeftShin_R13 LeftShin_tx LeftShin_R21 LeftShin_R22 LeftShin_R23 LeftShin_ty LeftShin_R31 LeftShin_R32 LeftShin_R33 LeftShin_tz LeftShin_M41 LeftShin_M42 LeftShin_M43 LeftShin_M44 LeftAnkle_R11 LeftAnkle_R12 LeftAnkle_R13 LeftAnkle_tx LeftAnkle_R21 LeftAnkle_R22 LeftAnkle_R23 LeftAnkle_ty LeftAnkle_R31 LeftAnkle_R32 LeftAnkle_R33 LeftAnkle_tz LeftAnkle_M41 LeftAnkle_M42 LeftAnkle_M43 LeftAnkle_M44 LeftFoot_R11 LeftFoot_R12 LeftFoot_R13 LeftFoot_tx LeftFoot_R21 LeftFoot_R22 LeftFoot_R23 LeftFoot_ty LeftFoot_R31 LeftFoot_R32 LeftFoot_R33 LeftFoot_tz LeftFoot_M41 LeftFoot_M42 LeftFoot_M43 LeftFoot_M44 JointRightThigh_R11 JointRightThigh_R12 JointRightThigh_R13 JointRightThigh_tx JointRightThigh_R21 JointRightThigh_R22 JointRightThigh_R23 JointRightThigh_ty JointRightThigh_R31 JointRightThigh_R32 JointRightThigh_R33 JointRightThigh_tz JointRightThigh_M41 JointRightThigh_M42 JointRightThigh_M43 JointRightThigh_M44 RightThigh_R11 RightThigh_R12 RightThigh_R13 RightThigh_tx RightThigh_R21 RightThigh_R22 RightThigh_R23 RightThigh_ty RightThigh_R31 RightThigh_R32 RightThigh_R33 RightThigh_tz RightThigh_M41 RightThigh_M42 RightThigh_M43 RightThigh_M44 RightShin_R11 RightShin_R12 RightShin_R13 RightShin_tx RightShin_R21 RightShin_R22 RightShin_R23 RightShin_ty RightShin_R31 RightShin_R32 RightShin_R33 RightShin_tz RightShin_M41 RightShin_M42 RightShin_M43 RightShin_M44 RightAnkle_R11 RightAnkle_R12 RightAnkle_R13 RightAnkle_tx RightAnkle_R21 RightAnkle_R22 RightAnkle_R23 RightAnkle_ty RightAnkle_R31 RightAnkle_R32 RightAnkle_R33 RightAnkle_tz RightAnkle_M41 RightAnkle_M42 RightAnkle_M43 RightAnkle_M44 RightFoot_R11 RightFoot_R12 RightFoot_R13 RightFoot_tx RightFoot_R21 RightFoot_R22 RightFoot_R23 RightFoot_ty RightFoot_R31 RightFoot_R32 RightFoot_R33 RightFoot_tz RightFoot_M41 RightFoot_M42 RightFoot_M43 RightFoot_M44
```

