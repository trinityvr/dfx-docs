# Trackman Integration Overview

Teams who want to leverage their internal Trackman datasets in DiamondFX can integrate within their shared DiamondFX account. Trackman
 assets are permissioned and made available within your organization exclusively and are hosted server-side.

---

## Trackman Integration with MySQL

DiamondFX supports ingestion of Trackman data hosted in a MySQL database.  Please complete the following to start integrating your assets with the DiamondFX platform.  

A database schema similiar to the following can be processed using our automated ingestion flow:

```

PitchNo int(10) UN 
Date date 
Time time 
PAofInning int(10) UN 
PitchofPA int(10) UN 
Pitcher varchar(256) 
PitcherId varchar(36) 
PitcherThrows char(10) 
PitcherTeam varchar(256) 
Batter varchar(256) 
BatterId varchar(36) 
BatterSide char(8) 
BatterTeam varchar(256) 
PitcherSet varchar(256) 
Inning int(10) UN 
Top_Bottom int(10) UN 
Outs int(10) UN 
Balls int(10) UN 
Strikes int(10) UN 
TaggedPitchType varchar(256) 
AutoPitchType varchar(256) 
PitchCall varchar(256) 
KorBB varchar(256) 
HitType varchar(256) 
PlayResult varchar(256) 
OutsOnPlay int(10) UN 
RunsScored int(10) UN 
Notes text 
RelSpeed float 
VertRelAngle float 
HorzRelAngle float 
SpinRate float 
SpinAxis float 
Tilt varchar(256) 
RelHeight float 
RelSide float 
Extension float 
VertBreak float 
InducedVertBreak float 
HorzBreak float 
PlateLocHeight float 
PlateLocSide float 
ZoneSpeed float 
VertApprAngle float 
HorzApprAngle float 
ZoneTime float 
ExitSpeed float 
Angle float 
Direction float 
HitSpinRate float 
PositionAt110X float 
PositionAt110Y float 
PositionAt110Z float 
Distance float 
LastTrackedDistance float 
Bearing float 
HangTime float 
pfxx float 
pfxz float 
x0 float 
y0 float 
z0 float 
vx0 float 
vy0 float 
vz0 float 
ax0 float 
ay0 float 
az0 float 
HomeTeam char(10) 
AwayTeam char(10) 
Stadium varchar(64) 
Level varchar(12) 
League varchar(12) 
GameId varchar(128) 
PitchUID varchar(528) PK 
pi_date_updated timestamp 
x55 float 
y55 float 
z55 float 
vx55 float 
vy55 float 
vz55 float 
Start_Speed55 float 
pi_pitch_type varchar(2) 
px double 
pz double 
pfx_x double 
pfx_z double 
pfx_xLONG double 
pfx_zLONG double 
top_zone float 
bot_zone float 
seq_number int(1) 
game_string varchar(26) 
x55_corr float 
y55_corr float 
z55_corr float 
vx55_corr float 
vy55_corr float 
vz55_corr float 
pfx_x_corr float 
pfx_z_corr float 
pfx_xLONG_corr float 
pfx_zLONG_corr float 
start_speed55_corr float 
px_corr float 
pz_corr float 
swing varchar(40) 
result varchar(40) 
outcome varchar(40) 
umpire varchar(40) 
catcher_mlbid varchar(10) 
player_position varchar(2) 
pi_pitch_group varchar(3) 
RelHeight_corr float 
RelSide_corr float 
Extension_corr float 
ax_corr float 
ay_corr float 
az_corr float 
RelSpeed_corr float 
VertRelAngle_corr float 
HorzRelAngle_corr float 
VertApprAngle_corr float 
HorzApprAngle_corr float 
SpinRate_corr float 
SpinAxis_corr float 
VertBreak_corr float 
InducedVertBreak_corr float 
HorzBreak_corr float 
ZoneSpeed_corr float 
SpinRateDerived float 
SpinRateDerived_corr float 
on_1b varchar(36) 
on_2b varchar(36) 
on_3b varchar(36)

```

### Create a MySQL User 

From the MySQL *Users and Privelages* utility, please create a new user with limited privelages to access the trackman data.  The following privelages should be granted:

- SELECT

Please permission the user to access the database from the following IP addresses if your database requires whitelisting:

```
TESTING 

    76.91.196.206

DEVELOPMENT

    54.202.2.222

PRODUCTION

    54.202.2.222
```

### Provide Database Credentials

Once the user has been created, please forward your database credentials (user, password, hostname, port) to your Customer Success Manager.

---

## Trackman Integration with CSV

If MySQL database access cannot be provided, DiamondFX supports one-time ingestion from a .csv file.   

### Export Trackman Table to CSV

SELECT the desired Trackman data time period for data export, run the query, and export the table to .csv

### Share CSV in a ZIP on BOX

Once the data has been exported, please create a zip folder with the file and forward to your Customer Success Manager for integration with your account.

---

## FAQ

- Do you support other database engines ?  *Not at this time, please refer to Trackman Integration with CSV*
- How often do you refresh Trackman data in the app ? *Trackman data is currently refreshed at daily periodicity for MySQL integrations*
- Do you support automated ingestion / refreshing of CSV files ?  *Not at this time, please provide MySQL access if you require frequent refreshing of Trackman data*

