![Screenshot](img/diamondFX-transparent.png)


# Welcome to the DIAMONDFX Documentation Portal

In the following pages, you'll find everything you need to get up and running with the DIAMONDFX platform.
Using the DIAMONDFX platform is a **3-step process across 3 discrete applications**.
We recommended reading each application's documentation page, **starting chronologically**, to understand the full scope of the platform.

## Introduction to DIAMONDFX

DIAMONDFX is an immersive **batting simulation** and **pitch recognition** training platform for developing player talent and identifying high value recruits through normalized, high accuracy data collection and predictive analysis.  

DIAMONDFX combines Virtual Reality (VR), Artificial Intelligence (AI), and historical pitching data to accurately simulate the batting experience against Major League pitching talent.

## DIAMONDFX Applications

*  [Admin Portal](admin_portal.md) - We'll show you how to **create and deploy simulations** from your web browser
*  [Simulation Client](sim_client.md) - We'll walk you through the **simulation flow and tools** available in the desktop VR application
*  [Analytics Portal](analytics_portal.md) - We'll demonstrate the types of data, transformations, and filters you can apply to **analyze simulation sessions**

## Key Resources

*  [DiamondFX Home Page](https://portal.trinity-dfx.com)
*  [DiamondFX Downloads](https://portal.trinity-dfx.com/#/account)
*  **Support Email** : support@trinityvr.com


