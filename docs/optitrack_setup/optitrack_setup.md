OptiTrack Setup
===============

For detailed instructions on how to setup OptiTrack, please refer to Cinder's OptiTrack Setup Guide (TODO ADD LINK HERE).  The DFX Installation Guide will serve as a high-level overview while emphasizing important details and caveats.

Required Hardware
-----------------

In addition to all of the hardware listed in the [SteamVR Setup](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.p1no2cage4mc) section, the following hardware (listed as "item x quantity") is required for Batting Simulation with OptiTrack:

1.  Laptop/PC ("Laptop #2", for Motive)

2.  Prime13 Camera x4

3.  Tripod/Wall mount x2 (total x4)

4.  Dual tripod ball head mount x2

5.  7-Port PoE Gigabit Switch

6.  CW-500 Calibration Wand

7.  CS-250 Calibration Square

8.  eSync2 Module

9.  3.5mm male to 3.5mm male aux cable (6ft or longer)

10. RCA to BNC adapter

11. Ethernet cable x8 (x4 for Cameras, x1 for eSync2, x2 for Laptop #2, 1x for Laptop #1)

12. USB Motive Hardware Key

13. Vive Tracker 2.0 (blue logo)

14. 1 USB 3.0 -> 3 USB 3.0 + Ethernet adapter x1 (add another x1 if Laptop #2 has no onboard Ethernet)

15. Bat (fixed markers) x2 OR Bat (no markers) + Bat Sleeve x2

Hardware Setup
--------------

### System Overview

#### Figure 3: DFX + Motive Network Topology![](https://lh5.googleusercontent.com/iloYL5vOuuOuTUYrd-NAPhiQ3xSaIPQmJw-_XgEMZtuGJDgk8rWoYR0POuNEM4hJ-tEvcGVpuZQEXKWCnbWkPqdR6Xw-ZnI31km5k4ztfGXMST56XFSR1u5NF9oa81GBHD9HS9nV)

### Instructions

Mount Cameras to tripods (or wall mount, depending on setup) such that each camera is at a slightly different height (e.g. 90" +- 6"), if possible. Refer to [Figure 4](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.jpybpq8f6n68) for an example room setup diagram for a 15ftx14ft tracking space.  Note that exact sensor placement will depend on room size/shape and does not need to exactly follow the diagram below.  If using dual tripod ball head mounts to attach 1 Camera and 1 Lighthouse to a single tripod/mount, then the minimum distance of 14"/16" in the diagram does not apply.

Place eSync2 module in the same corner as one of the Vive Lighthouses.  Connect 3.5mm aux cable from Lighthouse aux out to red port of RCA->BNC adapter.  Connect BNC side of adapter into Input1 on the eSync2 module.

Angle each camera's Field of View such that it can see the floor of the center of the playspace (i.e. the tracking volume's "origin") in the bottom of its field of view, as close to the bottom of the frame as possible.  This ensures we are not wasting any tracking volume that could be used to track the markers when held ~6ft above the ground, while having no "dead zones" near the ground.

Connect Ethernet cables from Prime13 Cameras+eSync2 to PoE switch.  Connect ethernet cable from PoE switch to laptop/PC onboard Ethernet port.  Plug in HW key (Motive USB drive) to laptop/PC and copy license key to License Folder if necessary.  Launch Motive and ensure license is validated and all 5 devices are detected in the 'Devices' pane.  Assemble bat (or put on bat sleeve, as appropriate).  Assemble CW-500 (calibration wand) + CS-200 (calibration triangle) with 3 markers each, if necessary.

#### Figure 4: Example SteamVR+OptiTrack Room Setup

![](https://lh4.googleusercontent.com/s4hZsLlqBCgo1A0cbHWC6E2tZ-guPoZCUXlV34E7ov6-fZ1X-bCLcVcySGJE1T3Q8MtbdZxkI_9PFIAaF83P160DrJcJYX9-uG3Id-rrAXHrLvAGG1bQ7E3ksxNJAco97W0mebbD)

### Multiple Laptop Configuration

If using 2 laptops, connect an Ethernet cable from the Motive Ethernet hub -> the on-board Ethernet of Laptop #2.  Connect one USB -> USB + Ethernet dongle to a USB 3.0 port (left-side ports ONLY on MSI laptop), plugging in Hardware Key in one of the USB ports.  Connect another Ethernet cable from this Ethernet dongle on Laptop #2 -> On-board ethernet on Laptop #1.

For each appropriate network device used to connect the two laptops (named "Realtek USB GbE Family Controller" if using Ethernet dongle, and "Qualcomm Atheros" or some other Ethernet controller name if using on-board Ethernet, do NOT use the primary network used for Internet), configure each with a unique Static IP address that is NOT on the regular network's subnet (e.g. if regular network is 192.168.1.*, use something like 192.168.200.100 for Laptop #1 and 192.168.200.101 for Laptop #2).  Example network adapter names are shown in [Figure 5](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.tnzqzdl9xth8) labeled "OptiTrack Cameras" (network for Cameras -> Laptop #2) and "Motive Data" (network for Laptop #2 -> Laptop #1) To do this, press Windows Key -> search for "Network status" -> "Change adapter options" (or Control Panel\Network and Internet\Network Connections on Win7), right click the appropriate adapter -> "Properties" -> click on "Internet Protocol Version 4 (TCP/IPv4)", then click "Properties" again.  Fill in all the settings as pictured in [Figure 6](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.q4zh0xsq7gxk) below, possibly changing the IP addresses if they are already taken (but ensure both laptops are on the same subnet, e.g. 192.168.200.*).  Then click "OK" on every open prompt to save the settings.  

#### Figure 5: Windows Network Connections Menu

![](https://lh6.googleusercontent.com/TQ7e57g4pORcbqlDhUNkKR-9SkGB27EwiKin__u-n-Sw0KJcjD7UwU6cqnjLlAV4ine31VSVmvqwlvNIAa88f2UhMLDXndt7_Cygdl0T6OEUknNOwwsy9Xa62E037UbDpUYzdgFS)

#### Figure 6: TCP/IP Settings For Motive Streaming Data Network

![](https://lh3.googleusercontent.com/20Ui_1PoDMh7QdUesfEQfCWctzHmq7bJiquep9DmPQlyeKSJAPLzyYsFpctznwtXbAQtWkMPZK-rDRal06w2rmcWZgHfwagj03c26Hcg-g7uztwEhcgFIV_vcXuHHmSohdlvfrqz)

Once this is complete, open "Data Streaming Pane" in Motive on Laptop #2 and update the network interface to point to the local IP address we just created (e.g. 192.168.200.101).  Now Motive will be streaming data on that network adapter instead of the primary one.

To verify the connection, try to connect to the Server (Laptop #2) from the Client (Laptop #1) using the NatNet Managed Client Sample in Unicast mode and using the two IPs we just created for Local/Server.  If data is streaming in using the correct Local/Server addresses, then the configuration is correct.

Once this config is verified to be good, open the SimConfig (i.e. C:\TrinityVR\DFX Suite\DFX Suite_Data\StreamingAssets\simConfig_prod.xml) and update the values for "LocalAddress" (i.e. address of Laptop #1) and "ServerAddress" (i.e. address of  Laptop #2) with the same IPs entered in the previous step.  Now, DFX (running on Laptop #1) will be able to stream in Motive data from Laptop #2 over a dedicated, isolated network connecting both laptops.

On Laptop #1, add a Windows Firewall Inbound Rule for TCP ports 1510-1511 (Motive command/data ports) and enable all ports from within DFX Suite. Navigate to "Windows Defender Firewall" (or Control Panel\System and Security\Windows Defender Firewall) and choose "Advanced  Settings" -> "Inbound  Rules" -> "New  Rule".  Select "TCP", and under "Specific  local ports" enter "1510-1511".  Select "Allow  the connection", then on the next screen, ensure all 3 of "Domain", "Private", and "Public" are selected.  Finally, give the rule a descriptive name (e.g. "Motive") and select "Finish".  The rule will now be visible in the "Inbound  Rules" list.  Similarly, check for any "DFX  Suite" rules, and ensure they are all "Allowed" for "Private" and "Public" networks as well.  This rule will be created automatically the first time DFX is started, so if you see no entries, launch DFX first and then ensure the entries are correct.  Restarting your PC may be necessary for changes to take effect.

#### Figure 7: Windows Firewall Inbound Rule Properties

![](https://lh4.googleusercontent.com/xdKEqegvovl50A4BVlLbqzVymQc4u14UUPberfjjoBKZEQ6OmHXA1KbWq7wW0t5gy9JlIZa5Of7CVCRag5J9OmUVFiqqla2GYrWx1Ikha5DQSArC43XFCvbY7y-XukBQRaK5dkz1)

On Laptop #2, enable the discrete graphics card in Nvidia Control Panel by using the settings in the below figure.

#### Figure 8: Nvidia Control Panel 3D Settings Pane

![](https://lh4.googleusercontent.com/Q6Gw7LiMdjgUiOF3cfDhjA_KqyEKjXG62YFcay_bq35h-po4sAvb-91yFBdC9WYP69HdpEyD4LI6iLYOzVA8ASKtNEVd1czyTwV69yT_XO3YfIkVTtUag1UA5mU8lfA6O32v674U)

Configuration
-------------

Load Motive 2.1.0 (or newer) and select "Layout" -> "Calibrate".  Load pre-supplied rigid body file (XXX.tra) containing rigid bodies for "Bat" and "Triangle" by selecting it with "File" -> "Open".  Check that all of the following application settings are set:

1.  Data Streaming Pane

1.  Broadcast Frame Data - On

2.  Local Interface - Set to correct interface setup in Section X

3.  Transmission Type - Unicast

4.  Command Port - 1510

5.  Data Port - 1511

3.  Camera Properties Pane

1.  Camera Frame Rate - 240 Hz

2.  Camera Exposure - 80-100us (lower is better)

3.  Camera Threshold - 200 (out of 255)

4.  Camera Gain - 1 (higher counteracts lower exposure but adds more noise)

5.  Continuous Calibration - On

5.  eSync2 Properties Pane

1.  Source - Input 1

2.  Input Multiplier - 4

3.  Source Frequency - Should be 200 Hz

7.  Rigid Body Properties Pane

1.  Min Marker Count - 3

2.  Min Deflection - 4mm

3.  Smoothing - 0

4.  Forward Prediction - 0

5.  Tracking Algorithm - Ray Based

6.  Unique - On

7.  Jump Prevention - Off

Refer to Figure 9, Figure 10 and Figure 11 below for the correct settings in Motive GUI.

#### Figure 9: Motive Data Streaming Pane

![](https://lh5.googleusercontent.com/rM6ffbvft3n6vAWwsdHXAH_sUT0W0AbBm970azV5HyXMXlGcBI990rOcTSFCg10_8SxkV2E_Etg5pi2ysr6JfV9z30gqDRcBTX5T0yO6IrHK2oMyfFskysiDM7T-kIzZPis-3RZp)

Figure 10: Motive Camera Properties Pane

![](https://lh6.googleusercontent.com/kR4lzIUsW5YF8kAaAvXyUQZ8gRrrmhc1Sq8qGXxFbMh8qzKS1wTw0DgBpTjiq73hEk9ZsScmGqPbMOwntEBLTqlfZEbVylY_hS48qoNYtdHtoV6AVmlfF-HGwEGlo01g8vp58iyW)

#### Figure 11: Motive Rigid Body Properties Pane

![](https://lh5.googleusercontent.com/Jm1gbxbN-iUuHSxMi3h7udYqOuKQ6mCFaunP0vkdKOLlkJ_DCS921Ta61VBT57ofVOAdoxj5JWbyV4hWVK4TIHOagXvb6hyxcFBXN14MW32nPKQPZJCxT8rQ6FxmZta4g8BA0skB)

Calibration
-----------

### Cameras/Ground Plane

Change Layout in Motive to "Calibration".  Mask out any sources of interference, either with "Clear  Mask" + "Mask  Visible" (after removing all markers from view), or manually with the Camera Preview tools.  After masking is complete, move CW-500 in straight lines (ensure markers are always facing upward) at varying heights and locations throughout the tracking volume, avoiding camera occlusions due to your body whenever possible.  When at least 3000 samples have been gathered for each camera, hit "Calculate", and ensure the resulting calibration result is "Exceptional" with < 0.4mm "Worst  Camera Mean 3D Error".  

Then, place CS-250 (Calibration Triangle) in center of playspace, and click "Refine  Ground Plane" with a vertical offset of y=19mm.  When prompted, save "<your_calibration>.cal" (calibration file) to disk in an easily accessible folder, incase it needs to be reloaded later.  To reload this calibration later, select "File" -> "Open" and select the .cal file you wish to load.

#### Figure 12: Motive Camera Calibration Pane

![](https://lh4.googleusercontent.com/EFrjIOnDklyTLjZXsAyY9ZqVdoegooCW0L-yVQZ80jOIvwOITbzsk84FbnOmqTnNQUAvEry7ETChNmheG5_vExmfUL2LtgcRDZOxArBteowFaKloaeyW1j1E_25fMmZw6XskVypK)

### Rigid Body Refinement

Hold bat parallel to ground in center of playspace (visible to all cameras), click on Bat in 'Assets' pane, then navigate to 'Builder' -> 'Edit' -> 'Refine',  then rotate bat lengthwise (like a screwdriver) until progress bar fills up, 'Accept' changes if refined error is less than old error.  Ideally, this is ~0.4mm or less.

Place bat in center of tracking space pointing straight up, with handle on the ground.  Select the "Bat" Rigid Body in "Assets", then look at the "Info" pane to find it's Position.  Ensure that Position.Y is 0mm (+-5mm).  If not, with "Bat" selected, navigate to "Builder" pane -> "Edit", then translate the Rigid Body so that Position.Y = 0mm, then press "Apply".

#### Figure 13: Motive Builder Pane

![](https://lh4.googleusercontent.com/4gKVLykiwofupbjZ2Olf41vBCGKAPf9q4JibkhCZA-ATmCKDTWyGJxsejA4Oj4z655kSRySzQBbAQjUqapu754CmQtku7AaIagaMhrEsyXLpeAGUFWG9J9Uw7PnTtstirEr3df7J)

DFX Suite Setup
===============

OptiTrack -> SteamVR Calibration
--------------------------------

Power on Vive Tracker and pair in SteamVR.  Afix to Calibration Triangle with screw (can be taken from CW-500), then affix triangle to tripod, as seen in [Figure 14](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.pzb9trt02vm6).  Then, load DFX calibration scene ("Settings" -> "Calibrate  OptiTrack", requires "OptiTrack  Enabled" checked), after scene loads press 'C' a few times to clear existing calibration. Move tripod-triangle-puck combination to each of the 4 red spheres, ensuring that your body is not blocking any camera/lighthouse from viewing the calibration objects.  Each sphere will turn from red ("incomplete") -> blue ("waiting to stabilize") -> purple ("recording samples") -> green ("complete") throughout the course of calibration.

Once all 4 calibration spheres are green, the virtual Calibration Triangle should appear in the correct location with respect to virtual Vive Tracker.  If so, grab bat and ensure it tracks correctly as well, ensuring that it does not go through the floor. If so, re-do SteamVR floor calibration, as it has likely drifted and is causing mapping errors, and start over.  If not, head into the simulation and take a few swings!

#### Figure 14: Triangle/Puck/Tripod Assembly for DFX OptiTrack Calibration

![](https://lh5.googleusercontent.com/97TA2zPvrSqodXFyA6YoyrfCebweWksr4blk5CMFuSgxo2F4BjUUaAFC5YVsFvFPoWBoHJS4BY9tIkUDeFHe18d34r49278R7jbupH3fXpUjf0bazoIXmRLv2Sau301aYgRseVKE)

