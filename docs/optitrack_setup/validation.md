Validation Checklist
====================

SteamVR
-------

Ensure all device firmware has been updated, and the supersampling settings are correct for the target hardware (e.g. 168% SS for GTX1070).

OptiTrack
---------

The following list of signoff items should be checked following the completion of every installation.  This can also serve as an auditing checklist for existing installation quality. If any of the below items are not true, please refer to the relevant sections of the FAQ and/or this installation guide and repeat all steps until the checklist item passes.

1.  Vive HMD does not lose tracking (grey-screen pop in) when moving around playspace

1.  Check that eSync2 is enabled, and that all lighthouses are powered on and can see headset throughout tracking volume

3.  Bat tracking is smooth (not jumpy) throughout tracking volume

1.  See "Bat does not track accurately" FAQ

5.  All tracking spaces have properly aligned ground planes

1.  Check that bat ends are aligned with the floor correctly (other than margin of error for virtual vs. real bat length) and that HMD, when placed on ground, is aligned with floor correctly

7.  Bat tracking when bunting is accurate

1.  This ensures calibration matrix defined in DFX Calibration Scene is correct

9.  Bat tracking when swinging is accurate

1.  This ensures calibration matrix defined in DFX Calibration Scene is correct and timing delays are correctly applied

