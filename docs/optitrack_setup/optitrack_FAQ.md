FAQ
===

SteamVR
-------

-   SteamVR Error -'Compositor Not Found'

    -   Restart SteamVR, optionally power cycling link box by pressing blue button on and then off

    -   Restart PC

-   For any other SteamVR errors, try Googling the error code (e.g. "steamvr error code 123")

OptiTrack
---------

-   Motive shows 0 cameras connected

    -   Restart Motive

    -   Disconnect/Reconnect Ethernet cable going from PoE switch to PC

-   Camera Preview does not update / "No Incoming Data: Check Settings"

    -   Ensure eSync2 connections are correct (Lighthouse Aux out connected to Input 1)

    -   Select eSync2, then under "Properties" -> "Sync  Input Settings", ensure "Source" is "Input1", "Source  Frequency" is ~50Hz and "Input  Multiplier" is 4, to make the effective "Camera  Rate" 200Hz

    -   If "Source  Frequency" is 0Hz on "Input1", lighthouse aux out may be defective.  Try swapping out the lighthouse to see if this fixes it.

-   Bat does not track at all

    -   Ensure 'Rigid Body Streaming' is enabled in 'Application Settings'

    -   Ensure correct rigid bodies ('Bat' or 'Bat Sleeve') are enabled in 'Assets'

    -   Ensure correct network interface is selected in 'Data Streaming' and it is broadcasting in "Unicast" mode

-   Bat does not track accurately (jumpy or inaccurate movement)

    -   Check 'Camera Preview' for environmental noise and/or unwanted masked regions (red)

    -   Check 'Info' after selecting the Rigid Body and ensure at least 6/8 markers are being tracked at all times with < 1mm "Avg. Marker Error"

    -   Redo Rigid Body Refinement and ensure 'Refined Error' is lower

DFX
---

-   User gets kicked back to mode select with only "Batting  Practice" as an option

    -   An API exception was likely thrown, check the log file (found at "C:\Users\<your username>\AppData\LocalLow\TrinityVR\DFX Suite\output_log.txt") for any POST/GET failures or errors, and file a bug report.