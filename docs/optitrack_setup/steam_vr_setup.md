SteamVR Setup
=============

Required Hardware
-----------------

The following hardware (listed as "item x quantity") is required to run Pitch Recognition / Vision Modules with SteamVR:

1.  PC/Laptop ("Laptop #1", for running DFX)

2.  Vive Pro Kit

3.  Tripod/Wall Mount x2 (for Lighthouses)

Hardware Setup
--------------

### System Overview

#### Figure 1: SteamVR System Setup

![](https://lh6.googleusercontent.com/Emtvot44BN4og3WVnR53pWPo2S2myd48CZMFogdQ8Abf-ppt2L2Bx-9uAV-tAZcOASaBExlWgx1qMfp07WHROf4y-t3o9-DkEoUlfTkDY2eTX4zvzFP4K3M3kLSsay86Xc3bqIw3)

### Instructions

Follow all steps in the Vive Pro Setup Guide, including plugging in all cables and installing all device drivers once HMD is powered on and connected to PC/Laptop.  Mount lighthouses to tripods (or wall mount, depending on setup) placed in opposite corners at least 7ft high. Angle lighthouses such that they are pointing at the floor of the center of the playspace (i.e. the tracking volume "origin"). Refer to [Figure 2](https://docs.google.com/document/d/1Pmi8tyS_smtJDxJuRzytpgvtnTrqOAL1tKsFI8uPew0/edit#heading=h.c71injmakes8) for an example room setup diagram for a 15ftx14ft tracking space, with "B" and "C" denoting Lighthouses.  Note that exact sensor placement will depend on room size/shape and does not need to exactly follow the diagram. 

#### Figure 2: Example SteamVR Room Setup![](https://lh3.googleusercontent.com/EvzDo5XHPfkaQhiC9rPVHW8VtwzeUzkD2vJLVmW6PDH15DOG6uxZq8NB9cyHMXu0QXFd1numgHZzWGnizO2yIlrhB-o3gdUXmT5PrjbuM7mSS0B59egxh6eX3K3e4zrcZw4u0C3B)

Start SteamVR, making sure to perform all software and firmware updates (both for SteamVR and HMD/Lighthouse/Controllers/Trackers), restarting SteamVR and/or the PC as necessary.

Configuration
-------------

The following SteamVR Settings should be checked and verified for correctness:

1.  General

1.  "Launch SteamVR Home Beta at Startup" = DISABLED

3.  Audio

1.  "When SteamVR is active"

1.  "Set Playback device to" = HTC VIVE HDMI AUDIO

2.  "Mirror audio to device" = <EXTERNAL TV NAME HERE> (if applicable)

3.  "When SteamVR is exited"

1.  "Set Playback device to" = Speakers (or the regular default device)

5.  Video

1.  "Application Resolution" -> "Manual  Override" = ENABLED

2.  "Application Resolution" -> Supersample Slider = 168% (for GTX 1070)

7.  Bluetooth

1.  "Enable Bluetooth Communication" = ENABLED

9.  Developer

1.  "Developer" -> "Start SteamVR when an application starts" = ENABLED

2.  "Developer" -> "Pause rendering when headset is idle" = ENABLED

3.  "Direct  Mode" -> "Enable Direct Mode" (needs to be checked only once)

Calibration
-----------

Run SteamVR Room Setup, with the arrow indicating the direction of the pitcher's mound relative to home plate (which is at the playspace center).  Place the HMD on the floor while running any scene, and it should rest on the ground plane, but not not go through or float above it.

If floor calibration becomes off at any time later, it can quickly be re-calibrated.  Place HMD positioned parallel to ground, in desired "origin" of playspace, facing in the desired direction of the pitcher's mound, then click the button located at SteamVR Settings -> "Developer" -> "Room and Tracking"> -> "Quick Calibrate" (with "Large Space" selected).  This ensures the floor calibration is set correctly and the playspace is oriented properly.  This can also be used if the playspace needs to be rotated (i.e. by 90 degrees) to change which way the pitcher's mound is relative to home plate.

