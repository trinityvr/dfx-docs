
Software Installation
=====================

Required Applications
---------------------

The following applications must be installed in order to use DFX Suite:

1.  Windows Updates

1.  These should be started up front, as they take awhile to complete and may slow down application performance if left to run in the background.

3.  Google Chrome

1.  <https://www.google.com/chrome/>

5.  TeamViewer (14 or newer)

1.  <https://www.teamviewer.com/en-us/download/windows/>

7.  Nvidia Graphics Drivers (417.35 or newer)

1.  <https://www.geforce.com/drivers>

9.  Steam

1.  <https://store.steampowered.com/about/>

11. SteamVR

1.  Download from within Steam ('Library' -> 'Tools' -> 'VR' -> 'SteamVR')

13. Motive (2.1.0 newer)

1.  <https://optitrack.com/downloads/>

15. DFX Suite (0.9.089 or newer)

1.  <https://portal.trinity-dfx.com/#/downloads>

17. NatNet SDK (for Managed Client Sample) (3.1 or newer)

1.  <https://optitrack.com/downloads/developer-tools.html#natnet-sdk>

19. HWiNFO (newest version available)

1.  <https://www.hwinfo.com/download/>

After all software has been installed and loaded for the first time, Windows' "Antimalware Service Executable" will run in the background for some time (which can be monitored via Task Manager), so allow this to complete before using DFX as it will cause choppy gameplay until it finishes scanning.

Disabled Applications
---------------------

The following applications must be disabled or uninstalled:

1.  Tobii Eye Tracking

1.  Uncheck 'on' after "Open"ing from system tray icon, or uninstall Tobii entirely.  If a red light is flashing above the top of the screen, then Tobii is still on and can interfere with tracking -- always ensure it is off.

