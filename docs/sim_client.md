#Overview

The **Client Simulation** is the VR application the player experiences.  The simulation allows players to **simulate at bats** and execute **pitch recognition exercises** with a variety of different configuration options.

___
## Application Setup

---

### Prerequisites

#### Hardware
*   [VIVE VR Ready PC](https://www.vive.com/us/ready/)
*   [HTC VIVE](https://www.vive.com/us/enterprise/)
*   [VIVE Tracker](https://www.vive.com/us/vive-tracker/)
*   TrinityVR Bat Mount

#### Software
*   [Windows 10](https://www.microsoft.com/en-us/store/b/windows)
*   [Google Chrome Browser](https://play.google.com/store/apps/details?id=com.android.chrome&hl=en)
*   [VIVE software](https://www.vive.com/us/setup/)

---

### Download the App

![Screenshot](img/client_sim/downloads.png)

In order to run the application it must be installed from the DIAMONDFX [downloads](https://portal.trinity-dfx.com/#/downloads) page accessible from the [Admin Portal](https://portal.trinity-dfx.com).
If DIAMONDFX is not already installed on the PC, select the latest version from the [downloads](https://portal.trinity-dfx.com/#/downloads) page.

### Run the Installer

![Screenshot](img/client_sim/installer.png)

**Locate the .exe and run the installer**, it will install the application and create a desktop shortcut.

### Launch the App

![Screenshot](img/client_sim/desktop_icon.png)

Run the **DFX Suite.exe** from the **\TrinityVR** directory or desktop icon.

### Login to the App

![Screenshot](img/client_sim/login.png)

In order to use the app, an Analyst or Operator must use the same login as the [Admin Portal](https://portal.trinity-dfx.com).
Enter your **username** and **password** and select the **login** button to authenticate.

---

## Main Menu

After logging in, you can view the **Main Menu** screen.  From the **Main Menu** the Operator can navigate to the following:
 
*   [App Settings](sim_client/#app-settings): Change the App configuration settings 
*   [Simulation Mode](sim_client/#simulation-mode):  Execute the VR simulation defined in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation)
*   [Swing Analysis Mode](sim_client/#swing-analysis-mode-experimental):  Replay swings and hits from **Simulation Mode** using a granular 3D visualization tool

___

##  App settings

![Screenshot](img/client_sim/settings.png)


Select the **gear** icon in the upper left-hand corner of the screen to check the app settings.  Below is an explanation of each option:

*   **Quality** - By default the quality of the graphics is set to "fantastic".  If you're experiencing performance issues you can toggle different quality settings
*   **Stadium** - We support a few stadium models at this time.  Select different stadiums using the dropdown selector
*   **Pitcher Visualization** - We support multiple Pitcher Visualization modes, select the highest priority visualization enumeration
    *    **Animation** - *The default option renders a generic wind-up animation*
    *    **Biomech** - *This option will prioritize the visualization of per pitch biomechanics assets uploaded to your account*
    *    **Video** - *This option will prioritize the visualization of per pitch composited video assets uploaded to your account*

**Advanced - Developer Options**

*   **Data Recording** - Developer option only
*   **Swing Recording** - Developer option only
*   **Bat Extrapolation** - Developer option only
*   **Debug Logging** - Developer option only
*   **Restricted Mode** - Developer option only

---

## Simulation Start

![Screenshot](img/client_sim/main_menu.png)

Select the icon that reads **Simulation** from the main menu.

## Simulation Queue

![Screenshot](img/client_sim/sim_queue.png)

The **Simulation Qeueu** will show a list of the most recently created simulations in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation) associated with that account.  

Select one of the simulations and click the **launch** button from the queue to begin.

##### Analytics Quick View

Select one of the simulations and click the **analytics** button from the queue to launch a webpage showing the selected player's analytics profile.

### Simulation Queue Filters

Operators can filter the **Simulation Qeueu** to make past simulations more easily discoverable and manage multiple systems running in parallel.

#### Simulation Owner Filters 

Filter by who created the simulation via the *dropdown selector* above the queue.  

![Screenshot](img/client_sim/sim_queue-account_filter.png)

*   **Account-based Filtering:** Show all of the simulations created across the Organization / Account
*   **User-based Filtering:** Show only the simulations created by the User / Operator logged in to the App

#### Sim Queue Pagination

Operators can view historical simulations by using the *paging functions* below the Simulation Queue results.  

![Screenshot](img/client_sim/sim_queue-pag.png)

### Launch the Simulation

This will launch the application in the VR headset and the player will now be able to take control of the application.

![Screenshot](img/client_sim/game_menu.png)

There are a few player selectable options once they're in the **Simulation**.  These options are selectable via **gaze triggers**, meaning they will be selected by looking at the icon for up to 3 seconds.

---

## Batting Simulation

If this mode is selected by the player under the Operator's direction, the **Batting Simulation** mode will begin.  There are a few more options the player can select from this point forward:

![Screenshot](img/client_sim/batting.png)

*   **Batting Practice** - 60mph Fastball pitches simulated. Pitch trajectories are simulated but are not specific to the parameters defined in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation)
    *   *Operator Key Shortcut:* 1
*   **Pitch Sequence** - Random pitch sequence with the parameters defined in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation). Pitch trajectories are simulated
    *   *Operator Key Shortcut:* 2
*   **At Bat** - Simulates an at bat with the parameters defined in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation).  Pitcher behaviors and pitch trajectories are simulated
    *   *Operator Key Shortcut:* 3

Once an option is selected, they can perform the routine until the simulation expires.  

-   To return to the *Batting Mode* menu, the player can stare at the 'Main Menu' gaze detector directly behind them at home plate.
    *   *Operator Key Shortcut:* M


#### Instant Replays 

An instant replay can be invoked by an **Operator** or the **Player** after a swing or pitch has been thrown.  When triggered, instant replays are rendered at half speed and visualize the *pitch break*, *strike zone*, and *swing plane*.

-   As a **Player**, stare at the 'Replay' gaze detector on the opposite side of home plate when it's available to trigger the instant replay
    *   *Operator Key Shortcut:* R
    
###### Instant Replay Visualizations
*   **Swing Plane**:  The planar cutting section of the swing is rendered in replay to visualize the intersection of the bat and strikezone
*   **Pitch Break**:  The break line is rendered showing at what point the pitch breaks in its trajectory from the mound

---

## Pitch Recognition

If this mode is selected by the player under the Operator's direction, the **Pitch Recognition** mode will begin. There are a few more options the player can select from this point forward.

![Screenshot](img/client_sim/pitch_recog.png)

NOTE:  **Pitch Recognition** can only be performed using the **Wand** controller option from the **App Settings**

#### Pitch Recognition Options

![Screenshot](img/client_sim/pitch_recog_options.png)

Each of the simulation modes are configurable with the following set of options.  Players can toggle these options at the begining of each simulation routine using the *trigger* button on wand input device.

*   **Pitcher Avatar** - Toggleable **on/off**, if selected, the pitcher will be shown, if deselected the pitcher will not be visible
*   **Pitch Break** - Toggleable **on/off**, if selected, the pitch break indicator will be shown with each pitch, if deselected the pitch break indicator will not be visible
*   **Pitch Speed** - Selectable, this defines the pitch speed as percentage of the actual speed, options are as follows: *100%, 75%, 50%, 25%*
*   **Pitch Trajectory** - Selectable, this defines the pitch trajectory cut-off from home plate in feet, options are as follows: *40ft, 30ft, 20ft, 0ft*


#### Strike Identification

If selected, this mode will start a **Strike Identification** routine for the player to complete.  The goal of the routine is for the player to call as many balls correctly in the session *(strikes, balls)*.
The # of pitches, pitcher, and pitch types seen in this mode are configured in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation) portal.

![Screenshot](img/client_sim/pitch_id_selector.png)

Players select their answers using the **trackpad wheel** on the wand controller.

#### Area Identification

If selected, this mode will start a **Area Identification** routine for the player to complete.  The goal of the routine 
is to build strikezone awareness by having the player correctly identify the area of the strike zone the ball intersected with *(left zone, middle zone, right zone, out of the zone)*.
The # of pitches, pitcher, and pitch types seen in this mode are configured in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation) portal.

![Screenshot](img/client_sim/area-id_2.png)

Players select their answers using the **trackpad wheel** on the wand controller.


#### Pitch Type Identification

If selected, this mode will start a **Pitch Identification** routine for the player to complete.   The goal of the routine is for the player to call as many balls correctly in the session *(fastballs, curveballs, sliders, changeups)*.
The # of pitches, pitcher, and pitch types seen in this mode are configured in the [Sim Admin](https://portal.trinity-dfx.com/#/simulation) portal.

![Screenshot](img/client_sim/pitch_type_selector.png)

Users select their answers using the **trackpad wheel** on the wand controller.

---

## Hotzones 

Hotzones are displayed at the end of every simulation session.  By default, hotzones display _chase rates by zone_ in [Batting Simulation](sim_client/#batting-mode) and _accuracy by zone_ in [Pitch Recognition](sim_client/#pitch-recognition).  

![Screenshot](img/client_sim/hotzones.png)


---

## Device Hot Swapping

Multiple tracking device types are supported for different exercises.  Tracked devices are automatically toggled as **active**
based on *proximity to **home plate.***  

####Supported Devices

*   **Controller** -  Use this device type for [Pitch Recognition](sim_client/#pitch-recognition) mode 
*   **Tracking Puck** -  Use this device type for [Batting Simulation](sim_client/#batting-mode) mode

**Note:**  Devices can only be swapped **prior to starting an exercise or mode.**  Once the mode/exercise begins, that device is locked as the **active device.**

---

## Changing Modes

To exit [Simulation Mode](sim_client/#simulation-mode) or [Swing Analysis Mode](sim_client/#swing-analysis-mode-experimental), the **Operator** can press the *escape* key to return to the main menu.  

___

## Swing Analysis Mode (Experimental)

In **Swing Analysis Mode**, an *Operator* or *Analyst* can view granular swing performance data.  All swings recorded in [Simulation Mode](sim_client/#simulation-mode) can be replayed and viewed from any angle in the Swing Analysis tool.

![Screenshot](img/client_sim/swing_analysis/swing_analysis.png)

After launching the application from the [Main Menu](sim_client/#main-menu), the Operator can retrieve and replay swings using the following filters and functions.

### Swing Filters

Swings can be filtered based on predefined session variables.  The following filters are available:

![Screenshot](img/client_sim/swing_analysis/swing_filters.png)

*   **Batter Name:**  Dropdown selector to filter swings by a Batter's name
*   **Pitcher Name:** Dropdown selector to filter swings by a Pitcher's name
*   **Swing ID:** Dropdown selector to filter swing by the Swing ID recorded in the database
*   **Input Type:** Dropdown selector to filter swing by the input type, the input types map to the following:
    *   *Puck:* Bat puck attachment using the VIVE tracker
    *   *Wand:* Wand controller device
    *   *Mount Rev II:* Discontinued

### Playback Options

There are 3 different swing sampling methods that can be made visible during playback.  They can be rendered simultaneously or individually for swing analysis 

![Screenshot](img/client_sim/swing_analysis/playback_options.png)

*   **RAW:** Toggleable on/off, if selected, the *RAW swing data* will be visible during playback
*   **Interpolated:** Toggleable on/off, if selected, the *interpolated swing data* will be visible during playback
*   **Extrapolated:** Toggleable on/off, if selected, the *extrapolated swing data* will be visible during playback

**NOTE:**  All derived metrics are captured using the *extrapolated* method

### Playing Back the Swing

To play back the swing, select the *Play* icon. 

![Screenshot](img/client_sim/swing_analysis/play_button.png)

### Pausing the Swing

To pause the swing at any time, select the *Pause* icon.

![Screenshot](img/client_sim/swing_analysis/pause_button.png)

### Adjusting Playback Speed

To adjust the playback speed of the swing, drag the *Playback Speed* input slider to the desired percentage.  

![Screenshot](img/client_sim/swing_analysis/playback_speed.png)

### Incrementing Swing Samples

To precisely increment between swing samples, use the *right* or *left* arrow icons while paused.  Samples can also be jumped to by setting the *Sample ID* input slider to the desired sample.  

![Screenshot](img/client_sim/swing_analysis/swing_increment.png)

___

## Operator Key Shortcuts

To provide the system Operator with additional control, most features can be interacted with using simple key commands.  Below is a list of Operator hot keys across all features in *Sim Client*.

### Batting

**Global Options:**

-   'S' : Trigger Global Reset
-   '[' : Decrease Render Scale
-   ']' : Increase Render Scale
-   'ESC' : Pause game and show menu

**When menu is open:**

-   '1' = Start Batting Practice Mode
-   '2' = Start Pitch Sequence Mode
-   '3' = Start At Bat Mode

**In a game mode:**

-   'R' = Trigger Replay
-   'M' = Open Main Menu

**Change Batting stance:**

-   'Left Arrow' = Left Hand Stance
-   'Right Arrow' = Right Hand Stance


### Pitch Recognition

**When menu is open:**

-   '1' = Start Strike ID Mode
-   '2' = Start Pitch ID Mode

**Answer Controls:**

*Two choices (Strike ID)*:

-   '1' - Select Top Answer
-   '2' - Select Bottom Answer

*Four choices (Pitch ID)*:

-   '1' - Select Top Answer
-   '2' - Select Left Answer
-   '3' - Select Right Answer
-   '4' - Select Bottom Answer

### Swing Analysis

**Global Controls:** 

-   'V' - Toggle VR Support/Rendering
-   'ESC' = Pause game and show menu

**Swing Controls:**

-   'P' - Toggle Play/Pause Swing
-   'R' - Restart Swing
-   '+' - Increase Playback Speed
-   '-' - Decrease Playback Speed
-   'Left Arrow' - Skip Backwards (Decrement Sample ID)
-   'Right Arrow' - Skip Forwards (Increment Sample ID)


### Login/Mode Select

**Global Controls:**

-   'S' - Toggle Settings Window On/Off
-   '1' = Load/Resume Batting Mode
-   '2' = Load/Resume Analysis Mode

Additional documentation is in progress.  Please contact **support@trinityvr.com** for support.

