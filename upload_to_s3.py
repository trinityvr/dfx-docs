# Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file
# except in compliance with the License. A copy of the License is located at
#
#     http://aws.amazon.com/apache2.0/
#
# or in the "license" file accompanying this file. This file is distributed on an "AS IS"
# BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under the License.
"""
A BitBucket Builds template for deploying an application revision to AWS CodeDeploy
narshiva@amazon.com
v1.0.0
"""
from __future__ import print_function
import os
from os import walk
import sys
import argparse
import boto3
from mimetypes import MimeTypes
from botocore.exceptions import ClientError

def upload_to_s3(bucket):
    """
    Uploads an artefact to Amazon S3
    """
    try:
        client = boto3.client('s3')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    try:
        uploadDirectory(bucket, client)

    except ClientError as err:
        print("Failed to upload artefact to S3.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access artefact in this directory.\n" + str(err))
        return False
    return True


def uploadDirectory(bucketname, client):
    path = os.path.dirname(os.path.abspath(__file__))
    path += "/site"
    for root, dirs, files in os.walk(path):
        for file in files:
            folder_path = os.path.dirname(os.path.join(root,file))
            path, folder_name = os.path.split(folder_path)

            if(folder_name !="build"):
                bucket_folder = os.path.relpath(root, os.path.dirname(os.path.abspath(__file__)))
                #bucket_folder += "/docs"
                bucket_key = bucket_folder + "/" + file
                bucket_key = bucket_key.split("/",1)
                bucket_key = bucket_key[1]
                print(bucket_key)
                print(file)
                write_object(os.path.join(root, file), bucketname, bucket_key, client)

            else:
                bucket_key = file
                bucket_key = bucket_key.split("/", 1)
                bucket_key = bucket_key[1]
                write_object(file, bucketname, bucket_key, client)


def write_object(object, bucket, bucket_key, client):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket)
    mime = MimeTypes()
    mime_type = mime.guess_type(object)
    print(mime_type[0])
    if(mime_type[0]!=None):
        bucket.upload_file(object, bucket_key, ExtraArgs={'ContentType':mime_type[0],'ACL':'public-read'})
    else:
        bucket.upload_file(object, bucket_key, ExtraArgs={'ACL':'public-read'})


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("bucket", help="Name of the existing S3 bucket")
    args = parser.parse_args()

    if not upload_to_s3(args.bucket):
        sys.exit(1)

if __name__ == "__main__":
    main()

